package com.lesson;

import java.util.Arrays;
import java.util.Scanner;

public class Task11 {
    /*
    * 1. Дан список книг. Книга содержит название, автора и год издания. Необходимо: найти самую старую книгу и вывести ее автора;
     * найти книгу определенного автора и вывести ее название (или названия); найти книги, изданные ранее введенного года и
     * вывести всю информацию по ним.

     1.1* Дополнение к первому заданию - книга дополнительно содержит издательство, может содержать нескольких авторов.
     Необходимо: найти книги определенного автора, написанные в соавторстве с кем-то (т.е. он не единственный автор книги) и
     вывести всю инфу по ним; найти книги, где 3 и более авторов и вывести всю инфу по ним.

    * */

    static class Book {
        String name;
        String[] author;
        int year;
        String publishing;

        Book(String nameBook, String[] authorBook, int yearBook, String publishingBook) {
            this.name = nameBook;
            this.author = authorBook;
            this.year = yearBook;
            this.publishing = publishingBook;
        }

    }


    // write your code here
    public static void main(String[] args) {
        Book[] books = {
                new Book("1984", new String[]{"Джордж Оруэлл 1", " Автор 2"}, 1948, "Издательство 1"),
                new Book("Тарас Бульба", new String[]{"Николай Гоголь"}, 1842, "Издательство 2"),
                new Book("Белая гвардия", new String[]{"Михаил Булгаков"}, 1924, "Издательство 3"),
                new Book("Приключения Шерлока Холмса", new String[]{"Артур Конан Дойль"}, 1927, "Издательство 4"),
                new Book("Триумфальная арка", new String[]{"Эрих Мария Ремарк"}, 1945, "Издательство 3"),
                new Book("Старик и море", new String[]{"Эрнест Хемингуэй 1", "Автор 2 ", "Энест 3"}, 1952, "Издательство 4"),
                new Book("Тихий Дон", new String[]{"Михаил Шолохов"}, 1940, "Издательство 2"),
                new Book("1985", new String[]{"Джордж Оруэлл 3", " Автор 4"}, 1948, "Издательство 1")
        };

        findBookByCoAuthors(books);
        findBookWithThreeCoAuthors(books);

    }


    static void findBookByCoAuthors(Book[] books) {

        Scanner scan = new Scanner(System.in);


        System.out.println("Введите имя автора  для поиска: ");
        String findCoAuthor = scan.nextLine();

        int i = 0;
        int foundBook = -1;
        while (i < books.length) {
            if (books[i].author.length > 1) {
                for (int j = 0; j < books[i].author.length; j++) {
                    if (books[i].author[j].toLowerCase().indexOf(findCoAuthor.toLowerCase()) > -1) {
                        System.out.println("Книга найдена: " + books[i].name + ", автор: " + Arrays.asList(books[i].author) +
                                ". Год издания:" + books[i].year + " , издательство - " + books[i].publishing);
                        foundBook = i;

                    }
                }
            }
            i++;
        }

        if (foundBook < 0) {
            System.out.println("Книга не найдена!");
        }
    }

    static void findBookWithThreeCoAuthors(Book[] books) {

        int i = 0;
        int foundBook = -1;
        while (i < books.length) {
            if (books[i].author.length > 2) {
                System.out.println("Книга найдена: " + books[i].name + ", автор: " + Arrays.asList(books[i].author) +
                        ". Год издания:" + books[i].year + " , издательство - " + books[i].publishing);
                foundBook = i;

            }
            i++;
        }
        if (foundBook < 0) {
            System.out.println("Книга не найдена!");
        }


    }


}

