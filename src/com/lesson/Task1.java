package com.lesson;

import java.time.Year;
import java.util.Scanner;

/*
* 1. Дан список книг. Книга содержит название, автора и год издания. Необходимо: найти самую старую книгу и вывести ее автора;
 * найти книгу определенного автора и вывести ее название (или названия); найти книги, изданные ранее введенного года и
 * вывести всю информацию по ним.

 1.1* Дополнение к первому заданию - книга дополнительно содержит издательство, может содержать нескольких авторов.
 Необходимо: найти книги определенного автора, написанные в соавторстве с кем-то (т.е. он не единственный автор книги) и
 вывести всю инфу по ним; найти книги, где 3 и более авторов и вывести всю инфу по ним.

* */
public class Task1 {

    static class Book {
        String name;
        String author;
        int year;

        Book(String nameBook, String authorBook, int yearBook) {
            this.name = nameBook;
            this.author = authorBook;
            this.year = yearBook;
        }

    }


    // write your code here
    public static void main(String[] args) {
        Book[] books = {new Book("1984", "Джордж Оруэлл", 1800),
                new Book("Тарас Бульба", "Николай Гоголь", 1842),
                new Book("Белая гвардия", "Михаил Булгаков", 1924),
                new Book("Приключения Шерлока Холмса", "Артур Конан Дойль", 1927),
                new Book("Триумфальная арка", "Эрих Мария Ремарк", 1945),
                new Book("Старик и море", "Эрнест Хемингуэй", 1952),
                new Book("Тихий Дон", "Михаил Шолохов", 1940)

        };
        findOldestBook(books);
        //findBookByAuthor(books);
        //findBookBeforeYear(books);
    }


    static void findOldestBook(Book[] books) {

        int minAge = books[0].year;

        int i = 0;
        int oldestBook = 0;
        while (i < books.length) {
            if (books[i].year < minAge) {
                minAge = books[i].year;
                oldestBook = i;
            }
            i++;
        }
        System.out.println(" Самая старая книга: " + books[oldestBook].name + ", автор: " + books[oldestBook].author +
                ". Год издания:" + books[oldestBook].year);


    }

    static void findBookByAuthor(Book[] books) {

        Scanner scan = new Scanner(System.in);


        System.out.println("Введите название книги  для поиска: ");
        String findBookName = scan.nextLine();

        int i = 0;
        int foundBook = -1;
        while (i < books.length) {
            if (books[i].name.toLowerCase().indexOf(findBookName.toLowerCase()) >= 0) {
                foundBook = i;
                break;
            }
            i++;
        }

        if (foundBook >= 0) {
            System.out.println("Книга найдена: " + books[foundBook].name + ", автор: " + books[foundBook].author +
                    ". Год издания:" + books[foundBook].year);
        } else {
            System.out.println("Книга не найдена!");
        }
    }


    static void findBookBeforeYear(Book[] books) {

        Scanner scan = new Scanner(System.in);


        System.out.println("Введите год до которого искать книги: ");
        int findBeforeYear = Integer.parseInt(scan.nextLine());

        int i = 0;
        int foundBook = -1;
        while (i < books.length) {
            if (books[i].year < findBeforeYear) {
                System.out.println("Книга найдена: " + books[i].name + ", автор: " + books[i].author +
                        ". Год издания:" + books[i].year);
                foundBook = 1;
            }
            i++;
        }

        if (foundBook < 0) {
            System.out.println("Книги не найдены!");
        }


    }


}
