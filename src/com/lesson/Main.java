package com.lesson;

import java.util.Scanner;

public class Main {
    // write your code here
    public static void main(String[] args) {
        User users[];
        //users=new User[3];

        users = getUserData();
        // String resString = findEnglish(strings);
        printResult(users);
    }


    static void printResult(User users[]) {

        for (int i = 0; i < 3; i++) {

            System.out.println(" Результат поиска: " + users[i].userName + ":" + users[i].age);
        }


    }


    static String findEnglish(String[] strings) {

        String engLetters = "aeyuoqwrtpsdfghklzxcvbnm";
        int find;

        for (int i = 0; i < strings.length; i++) {
            find = engLetters.indexOf(strings[i].toLowerCase().charAt(0));
            if (find > 0) return strings[i];

        }
        return "";

    }

    static User[] getUserData() {
        Scanner scan = new Scanner(System.in);


        User[] users = new User[3];

        //Введите строку номер
        for (int i = 0; i < 3; i++) {
            System.out.println(String.format("Введите имя, пользователя %d: ", i + 1));

            User user = new User();
            user.userName = scan.nextLine();

            System.out.println(String.format("Введите возраст, пользователя %d: ", i + 1));
            user.age = Integer.parseInt(scan.nextLine());

            users[i] = user;
        }
        return users;

    }

    static class User {
        String userName;
        int age;
    }


}
