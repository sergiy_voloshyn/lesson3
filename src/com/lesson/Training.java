package com.lesson;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Training {

  /*
    1. Ввести текст с консоли. Подсчитать количество предложений в тексте, вывести предложение с максимальным количеством слов и предложение, которое содержит слово с максимальным количеством букв.

            2. Ввести n строк с консоли. Найти слово, в котором число различных символов минимально. Если таких слов несколько, найти самое длинное из них.

            3. Ввести n строк с консоли. Найти количество слов, содержащих только символы латинского алфавита, а среди них – количество слов с равным числом гласных и согласных букв.

4. Ввести n строк с консоли. Найти слово, состоящее только из различных символов. Если таких слов несколько, найти первое из них.

5. Написать программу, которая будет проверять, является ли введенное пользователем слово палиндромом. Если слово не читается одинаково в обоих направлениях, то выдавать пользователю ошибку.

5.1 Программа должна определять палиндром, даже если в слове есть буквы разного регистра (например “Шалаш”, “поТОП”).

            5.2 Определять не только слова, но и предложения. Учесть, что предложения могут содержать знаки пунктуации (например “Аргентина манит негра!”)
*/

    public static void main(String[] args) {

        countSentences();
        // findWordMinimalDiferentSymbols();
        //findWordFromLatinSymbols();
        //findWordFromDiferentSymbols();
        //testForPalindrom();
        // testForPalindromSentence();


    }

    static void countSentences() {
        //Ввести текст с консоли. Подсчитать количество предложений в тексте, вывести предложение с максимальным количеством слов
        // и предложение, которое содержит слово с максимальным количеством букв.

        Scanner scan = new Scanner(System.in);


        System.out.println("Введите строку: ");
        String inputString = scan.nextLine();

        String[] sentences = inputString.split("\\.");

        System.out.println("Количество предложений: " + sentences.length);

        int maxWordsCount = countSpace(sentences[0]);
        String maxWordsSentence = sentences[0];

        for (int i = 0; i < sentences.length; i++) {
            if (maxWordsCount < countSpace(sentences[i])) {
                maxWordsCount = countSpace(sentences[i]);
                maxWordsSentence = sentences[i];
            }
        }
        System.out.println("Предложение с максимальным количеством слов: " + maxWordsSentence);
        //предложение, которое содержит слово с максимальным количеством букв.

        //количество букв в самом большом слове в каждом предложении
        int[] maxChars = new int[sentences.length];

        for (int i = 0; i < sentences.length; i++) {
            String[] words = sentences[i].split(" ");

            int maxSymbols = words[0].length();

            for (int j = 0; j < words.length; j++) {
                if (maxSymbols < words[j].length()) {
                    maxSymbols = words[j].length();
                }
            }
            maxChars[i] = maxSymbols;
        }

        int bigWordCount = maxChars[0];
        int indexSentence = 0;
        for (int i = 0; i < maxChars.length; i++) {
            if (bigWordCount < maxChars[i]) {
                bigWordCount = maxChars[i];
                indexSentence = i;
            }
        }
        System.out.println("Предложение которое содержит слово с максимальным количеством букв: " + sentences[indexSentence].toString());
    }


    static int countSpace(String sentence) {
        //проверяем каждый символ в строке
        int count = 0;
        for (int j = 0; j < sentence.length(); j++) {
            if (sentence.toLowerCase().charAt(j) == ' ') ;
            count++;
        }
        return count;
    }


    static void findWordMinimalDiferentSymbols() {
        //2. Ввести n строк с консоли. Найти слово, в котором число различных символов минимально. Если таких слов несколько, найти самое длинное из них.
        Scanner scan = new Scanner(System.in);

        //количество строк
        System.out.println("Введите количество строк: ");
        int numRows = Integer.parseInt(scan.nextLine());


        String[] arrayOfRows = new String[numRows];

        //Введите строку номер
        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Введите строку номер %d: ", i + 1));
            arrayOfRows[i] = scan.nextLine();

        }

        int[] minDiffCount = new int[arrayOfRows.length];
        String[] minWord = new String[arrayOfRows.length];

        StringBuilder sameSymbols = new StringBuilder("");

        for (int i = 0; i < arrayOfRows.length; i++) {
            //разделить каждую строку на слова
            String[] words = arrayOfRows[i].split(" ");
            minDiffCount[i] = getCountDifferenrSymbols(words[0]);
            minWord[i] = words[0];

            for (int j = 0; j < words.length; j++) {
                //проверить каждое слово
                int currDiffSymbol = getCountDifferenrSymbols(words[j]);
                if (minDiffCount[i] > currDiffSymbol) {
                    minDiffCount[i] = currDiffSymbol;
                    minWord[i] = words[j];
                    sameSymbols.append(minWord[i]);
                }
            }
        }
        System.out.println("Cлова в котором число различных символов минимально: " + Arrays.asList(minWord));


        String maxLongString = minWord[0];
        int maxLength = minWord[0].length();

        for (int i = 0; i < minWord.length; i++) {
            if (maxLength < minWord[i].length()) {
                maxLength = minWord[i].length();
                maxLongString = minWord[i];
            }
        }

        System.out.println("Cлово в котором число различных символов минимально: " + maxLongString);

    }

    static int getCountDifferenrSymbols(String word) {
        StringBuilder differentSymbols = new StringBuilder("");
        for (int k = 0; k < word.length(); k++) {
            char currentChar = word.toLowerCase().charAt(k);
            //добавить новый символ
            if (differentSymbols.indexOf(String.valueOf(currentChar)) == -1) {
                differentSymbols.append(currentChar);
            }
        }
        return differentSymbols.length();
    }


    static void findWordFromLatinSymbols() {
        //3. Ввести n строк с консоли. Найти количество слов, содержащих только символы латинского алфавита, а среди них – количество слов
        // с равным числом гласных и согласных букв.

        Scanner scan = new Scanner(System.in);

        //количество строк
        System.out.println("Введите количество строк: ");
        int numRows = Integer.parseInt(scan.nextLine());


        String[] arrayOfRows = new String[numRows];

        //Введите строку номер
        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Введите строку номер %d: ", i + 1));
            arrayOfRows[i] = scan.nextLine();

        }

        int wordsLatin = 0;
        int wordsEqualChar = 0;
        String glasnye = "eyuioaj";
        String soglasnye = "qwrtpsdfghklzxcvbnm";

        for (int i = 0; i < arrayOfRows.length; i++) {
            //разделить каждую строку на слова
            String[] words = arrayOfRows[i].split(" ");

            for (int j = 0; j < words.length; j++) {
                //проверить каждое слово
                String word = words[j];
                boolean flagNotLatin = false;
                for (int k = 0; k < word.length(); k++) {
                    char currentChar = word.charAt(k);
                    //latin symbols
                    if ((currentChar > 65) & (currentChar < 90) | (currentChar > 97) & (currentChar < 122)) {
                        continue;
                    } else {
                        flagNotLatin = true;
                        break;
                    }
                }
                if (!flagNotLatin) {
                    wordsLatin++;
                    //подсчитать гласные и согласные
                    int glasnyeCount = 0;
                    int soglasnyeCount = 0;
                    for (int k = 0; k < word.length(); k++) {
                        char currentChar = word.charAt(k);
                        if (glasnye.contains(String.valueOf(currentChar).toLowerCase())) {
                            glasnyeCount++;
                        }
                        if (soglasnye.contains(String.valueOf(currentChar).toLowerCase())) {
                            soglasnyeCount++;
                        }
                    }
                    if (glasnyeCount == soglasnyeCount) {
                        wordsEqualChar++;
                    }

                }

            }
        }

        if (wordsLatin > 0) {
            System.out.println("Слова найдены: " + wordsLatin + " шт.");
            if (wordsEqualChar > 0) {
                System.out.println("Количество слов с равным числом гласных и согласных букв : " + wordsEqualChar);
            }
        } else {
            System.out.println("Слова не найдены!");
        }
    }


    static void findWordFromDiferentSymbols() {

        //4. Ввести n строк с консоли. Найти слово, состоящее только из различных символов. Если таких слов несколько, найти первое из них.
        Scanner scan = new Scanner(System.in);

        //количество строк
        System.out.println("Введите количество строк: ");
        int numRows = Integer.parseInt(scan.nextLine());


        String[] arrayOfRows = new String[numRows];

        //Введите строку номер
        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Введите строку номер %d: ", i + 1));
            arrayOfRows[i] = scan.nextLine();

        }

        boolean flagFound = false;
        exit:
        for (int i = 0; i < arrayOfRows.length; i++) {
            //разделить каждую строку на слова
            String[] words = arrayOfRows[i].split(" ");

            for (int j = 0; j < words.length; j++) {
                //проверить каждое слово
                char symbol = words[j].charAt(0);
                String word = words[j];
                for (int k = 0; k < word.length(); k++) {
                    if (word.charAt(k) != symbol) {
                        flagFound = true;
                        System.out.println("Слово найдено: " + word);
                        break exit;
                    }
                }
            }
        }
        if (!flagFound) {
            System.out.println("Слово не найдено!");
        }
    }


    static void testForPalindrom() {
            /*5. Написать программу, которая будет проверять, является ли введенное пользователем слово палиндромом. Если слово не читается одинаково в обоих направлениях,
            то выдавать пользователю ошибку.

            5.1 Программа должна определять палиндром, даже если в слове есть буквы разного регистра (например “Шалаш”, “поТОП”).*/

        Scanner scan = new Scanner(System.in);

        System.out.println("Введите строку: ");

        String inputString = scan.nextLine();


        StringBuilder reverseString = new StringBuilder(inputString.toLowerCase()).reverse();


        if (reverseString.toString().equals(inputString)) {

            System.out.println("Введенное слово является палиндромом:" + inputString);

        } else {
            System.out.println("Введенное слово не является палиндромом:" + inputString);


        }


    }

    static void testForPalindromSentence() {
        //5.2 Определять не только слова, но и предложения. Учесть, что предложения могут содержать знаки пунктуации (например “Аргентина манит негра!”)

        Scanner scan = new Scanner(System.in);

        System.out.println("Введите строку: ");

        String inputString = scan.nextLine();


        //Аргентина манит негра!

        String stringNoPunctuation = inputString.replaceAll("\\p{Blank}|\\p{Punct}", "");

        StringBuilder stringReverse = new StringBuilder(stringNoPunctuation.toLowerCase()).reverse();


        if (stringReverse.toString().equals(stringNoPunctuation.toLowerCase())) {

            System.out.println("Введенное предложение является палиндромом:" + inputString);

        } else {
            System.out.println("Введенное предложение не является палиндромом:" + inputString);


        }
    }


}




